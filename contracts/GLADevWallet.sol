// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/access/Ownable.sol";

contract DevWallet is Ownable{
    address gameManager;
    uint256 AMOUNT_APPROVED = 10**35;
    
    constructor(address _gameManager) Ownable(){
        gameManager = _gameManager;
    }

    function setApproveForBattle() public onlyOwner{
        address token = IGameManager(gameManager).getContract("GLAToken");
        address battle = IGameManager(gameManager).getContract("GLABattle");
        IGLAToken(token).approve(battle, AMOUNT_APPROVED);
    }
}

interface IGLAToken{
    function approve(address spender, uint256 amount) external returns (bool);
}

interface IGameManager{
    function  getContract(string memory contract_) external view returns (address);
}
