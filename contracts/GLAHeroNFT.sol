// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/proxy/utils/Initializable.sol";
contract GLAHeroNFT is ERC721Enumerable, Ownable, Initializable{

    using SafeMath for uint256;
    using Address for address;
    using Strings for uint256;
    
    struct Hero {
        uint256 heroId;
        uint types;
        string name;
        uint8 rarity;
        uint8 level;
        uint256 experience;    
        uint256 lastBattleTime;
    }

    address public gameManager;
    uint16  private LEVEL_2_THRESHOLD;
    uint16  private LEVEL_3_THRESHOLD;
    uint16  private LEVEL_4_THRESHOLD;
    uint16  private LEVEL_5_THRESHOLD;
    uint16  private LEVEL_6_THRESHOLD;
    string _name;
    string _symbol;
    uint256 renameFee;
    Counters.Counter  counter;
    uint8 private MAX_RARITY;

    mapping(uint=>string) nameOf; // hero type => hero name
    mapping(uint256 => Hero) private heroes; // hero ID => hero info

    event HeroUpgrade(uint256 indexed heroId_, uint8 rarity);
    event HeroDowngrade(uint256 indexed heroId_, uint8 rarity);
    event HeroLevelUp(uint256 indexed heroId_, uint8 level);
    event HeroSpawn(address indexed owner, Hero hero);
    event SetBattleTime(uint256 indexed heroId, uint256 newBattletime);
    event SetExp(uint256 indexed heroId, uint256 newExp);
    event SetName(uint256 indexed heroId, string name);

    constructor() ERC721("NFT: Galaxy Adventure", "NFT GLA") {

    }

    modifier only(string memory necessaryContract ) {
        require(IManageContract(gameManager).getContract(necessaryContract) == msg.sender, "not authorized");
        _;
    }
    
    modifier onlyOwnerOf(uint256 tokenId_) {
        require(ERC721.ownerOf(tokenId_) == msg.sender, "ERC721: You are not the owner!");
        _;
    }
    
    modifier onlyGameManager {
        require(gameManager == msg.sender, "not authorized");
        _;
    }

    function initialize(address gameManager_) public initializer {
        gameManager = gameManager_;
        Counters.reset(counter);
        nameOf[0] = "RAY";
        nameOf[1] = "JAX";
        nameOf[2] = "KAT";
        LEVEL_2_THRESHOLD = 990;
        LEVEL_3_THRESHOLD = 3490;
        LEVEL_4_THRESHOLD = 9990;
        LEVEL_5_THRESHOLD = 19990;
        LEVEL_6_THRESHOLD = 39990;
        MAX_RARITY = 6;
        _name = "NFT: Galaxy Adventure";
        _symbol = "GLA NFT";
    }
    function name() public view override returns(string memory){
        return _name;
    }
    function symbol() public view override returns(string memory){
        return _symbol;
    }
    // Called when buying heroes or open chest from spawner contract:
    function mint(address owner_, uint type_, uint8 rarity_ ) external  only("GLASpawner") {
        Counters.increment(counter);
        Hero memory hero = Hero(Counters.current(counter), type_, nameOf[type_], rarity_, 1, 0, 0);
        heroes[Counters.current(counter)] = hero;
        _mint(owner_,Counters.current(counter));
        emit HeroSpawn(owner_, hero);
    }
    
    function isApprovedForAll(address owner, address operator) public view virtual override returns (bool) {
        if (operator == IManageContract(gameManager).getContract("GLAMarket"))
            return true;
        else 
            return ERC721.isApprovedForAll(owner, operator);
    }
    
    function gainExp(uint256 heroId_, uint256 amount) external only("GLABattle")  {
        heroes[heroId_].experience =  heroes[heroId_].experience.add(amount);
        emit SetExp(heroId_, heroes[heroId_].experience);
        uint8 level = heroes[heroId_].level;
        if (heroes[heroId_].experience > LEVEL_6_THRESHOLD) {
            heroes[heroId_].level = 6;
        } else if (heroes[heroId_].experience > LEVEL_5_THRESHOLD) {
            heroes[heroId_].level = 5;
        } else if (heroes[heroId_].experience > LEVEL_4_THRESHOLD) {
            heroes[heroId_].level = 4;
        } else if (heroes[heroId_].experience > LEVEL_3_THRESHOLD) {
            heroes[heroId_].level = 3;
        } else if (heroes[heroId_].experience > LEVEL_2_THRESHOLD) {
            heroes[heroId_].level = 2;
        }
        if (level < heroes[heroId_].level) {
            emit HeroLevelUp(heroId_, heroes[heroId_].level);
        }
    }
    
    function getOwnerOf(uint256 heroId_) external view returns (address) {
        return ERC721.ownerOf(heroId_);
    }
    
    function getHero(uint256 heroId_) external view returns(Hero memory){
        return heroes[heroId_];
    }

    function getHeroName(uint256 heroId_) external view returns(string memory){
        return heroes[heroId_].name;
    }
    
    function getHeroExp(uint256 heroId_) external view returns(uint256) {
        return heroes[heroId_].experience;
    }

    function getHeroType(uint256 heroId_) external view returns(uint) {
        return heroes[heroId_].types;
    }

    function getHeroLevel(uint256 heroId_) external view returns (uint8) {
        return heroes[heroId_].level;
    }

    function getHeroRarity(uint256 heroId_) external view returns (uint8) {
        return heroes[heroId_].rarity;
    }


    function getLastBattleTime(uint256 heroId_) external view returns (uint256) {
        return heroes[heroId_].lastBattleTime;
    }
    
    function getListHeroesOf(address owner) public view returns(Hero[] memory){
        uint256 balance = balanceOf(owner);
        Hero[] memory listHero = new Hero[](balance);
        for (uint i=0; i<balance; i++){
            uint256 heroId_ = tokenOfOwnerByIndex(owner, i);
            listHero[i]=heroes[heroId_];
        }
        return listHero;
    }
    
    function getListHeroIdsOf(address owner) public view returns(uint256[] memory){
        uint256 balance = balanceOf(owner);
        uint256[]memory listHeroIds = new uint256[](balance);
        for (uint i=0; i<balance; i++){
            uint256 heroId_ = tokenOfOwnerByIndex(owner, i);
            listHeroIds[i] = heroId_;
        }
        return listHeroIds;
    }

    function setLastBattleTime(uint256 heroId_) external only("GLABattle") {
        uint256 newBattleTime = block.timestamp;
        heroes[heroId_].lastBattleTime = newBattleTime;
        emit SetBattleTime(heroId_, newBattleTime);
    }

    // Called by Upgrade contract:
    function upgradeRarity(uint256 heroId_) external only("GLAUpgrade") {
        if(heroes[heroId_].rarity < MAX_RARITY){
            heroes[heroId_].rarity ++;
            emit HeroUpgrade(heroId_, heroes[heroId_].rarity);
        }
    }

    // Called by Upgrade contract when upgradeRarity fails:
    function downgradeRarity(uint256 tokenId_) external only("GLAUpgrade") {
        if(heroes[tokenId_].rarity > 1){
            heroes[tokenId_].rarity -= 1;
            emit HeroDowngrade(tokenId_, heroes[tokenId_].rarity);
        }
    }

    function burn(uint256 tokenId_) public onlyOwnerOf(tokenId_) {
        delete heroes[tokenId_];
        _burn(tokenId_);
    }
    
    function setGameManager(address manageContract_) external onlyOwner{
        gameManager = manageContract_;
    }

    function setHeroName(uint256 heroId_, string memory name_) public onlyOwnerOf(heroId_) {
        if(renameFee>0){
            address glaTokenAddr = IManageContract(gameManager).getContract("GLAToken");
            address devWallet = IManageContract(gameManager).getDevWallet();
            IGLAToken(glaTokenAddr).transferFrom(msg.sender, devWallet, renameFee);
        }
        heroes[heroId_].name = name_;
        emit SetName(heroId_, name_);
    }
    
    function setRenamFee(uint256 newRenameFee) external onlyGameManager{
        renameFee= newRenameFee;
    }
    
}

interface IManageContract{
    function  getContract(string memory contract_) external view returns (address);  
    function  getDevWallet() external view returns (address);
}

interface IGLAToken{
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
}
