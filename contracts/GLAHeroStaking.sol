//  SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Enumerable.sol";

contract GLAHeroStaking is Ownable {
    using SafeMath for uint256;
    struct Plan {
        uint256 time;
        uint256 rewardRate;
    }

    struct HeroStakingData {
        uint256 heroId;
        address owner;
        uint256 plan;
        uint256 start;
        uint256 finish;
        uint256 rewardEstimate;
    }
    uint256 public constant BASE_REWARD = 128 * 10**18;
    uint256 constant STEP_TIME = 1 days;
    address public mintOperator;
    address public gameManager;
    mapping(uint256 => mapping(uint256 => uint256))
        public maxHeroStakedInPlanByRare;
    mapping(uint256 => mapping(uint256 => uint256))
        public currentHeroStakedInPlanByRare;
    mapping(address => uint256) public amountHeroStaking;
    mapping(address => mapping(uint256 => uint256)) listHero; //address => index => heroId
    mapping(uint256 => HeroStakingData) public heroesStakingData; //heroId => Hero staking data
    mapping(uint256 => uint256) stakedHeroIndex;
    mapping(uint256 => Plan) public plans;
    
    event StakeHero(address user, uint256[] heroIds, uint256 plan);
    event Withdraw(address user, uint256 heroId, uint256 glaAmount);

    constructor(address _gameManager, address _mintOperator) {
        plans[0] = Plan(30, 45);
        plans[1] = Plan(60, 110);
        plans[2] = Plan(90, 180);
        mintOperator = _mintOperator;
        gameManager = _gameManager;
        maxHeroStakedInPlanByRare[0][1] = 600;
        maxHeroStakedInPlanByRare[0][2] = 300;
        maxHeroStakedInPlanByRare[0][3] = 250;
        maxHeroStakedInPlanByRare[0][4] = 200;
        maxHeroStakedInPlanByRare[0][5] = 100;
        maxHeroStakedInPlanByRare[0][6] = 50;

        maxHeroStakedInPlanByRare[1][1] = 400;
        maxHeroStakedInPlanByRare[1][2] = 200;
        maxHeroStakedInPlanByRare[1][3] = 150;
        maxHeroStakedInPlanByRare[1][4] = 100;
        maxHeroStakedInPlanByRare[1][5] = 100;
        maxHeroStakedInPlanByRare[1][6] = 50;

        maxHeroStakedInPlanByRare[2][1] = 200;
        maxHeroStakedInPlanByRare[2][2] = 100;
        maxHeroStakedInPlanByRare[2][3] = 75;
        maxHeroStakedInPlanByRare[2][4] = 50;
        maxHeroStakedInPlanByRare[2][5] = 50;
        maxHeroStakedInPlanByRare[2][6] = 25;
    }

    function stakeHero(uint256[] memory heroIds, uint256 plan) public {
        uint256 totalRewardEstimate;
        for(uint256 i = 0; i < heroIds.length; i++){
            address glaHeroAddress = IGameManager(gameManager).getContract("GLAHeroNFT");
            address owner = IGLAHeroNFT(glaHeroAddress).ownerOf(heroIds[i]);
            require(
                owner == msg.sender,
                "GLAHeroStaking: Stake hero that is not own"
            );
            uint8 rare = IGLAHeroNFT(glaHeroAddress).getHeroRarity(heroIds[i]);
            require(
                currentHeroStakedInPlanByRare[plan][rare] <
                    maxHeroStakedInPlanByRare[plan][rare],
                "This plan is full!"
            );
            IGLAHeroNFT(glaHeroAddress).transferFrom(
                owner,
                address(this),
                heroIds[i]
            );
            uint256 rewardEstimate;
            rewardEstimate = plans[plan].rewardRate.mul(rare).mul(BASE_REWARD);
            totalRewardEstimate = totalRewardEstimate.add(rewardEstimate);
            heroesStakingData[heroIds[i]] = HeroStakingData(
                heroIds[i],
                owner,
                plan,
                block.timestamp,
                block.timestamp.add(plans[plan].time.mul(STEP_TIME)),
                rewardEstimate
            );

            stakedHeroIndex[heroIds[i]] = amountHeroStaking[owner];
            listHero[owner][amountHeroStaking[owner]] = heroIds[i];
            amountHeroStaking[owner] += 1;
        }
        IMintOperator(mintOperator).mint(address(this), totalRewardEstimate);
        emit StakeHero(msg.sender, heroIds, plan);

    }

    function withdraw(uint256 heroId) public {
        address glaHeroAddress = IGameManager(gameManager).getContract("GLAHeroNFT");
        address glaTokenAddress = IGameManager(gameManager).getContract("GLAToken");
        //now >= start time + plantime * 1 day
        require(
            block.timestamp >=
                heroesStakingData[heroId].start.add(
                    plans[heroesStakingData[heroId].plan].time.mul(STEP_TIME)
                ),
            "Please wait for hero to complete the mission"
        );
        require(
            heroesStakingData[heroId].owner == msg.sender,
            "GLAHeroStaking: Withdraw hero that is not own "
        );
        IGLAHeroNFT(glaHeroAddress).transferFrom(
            address(this),
            msg.sender,
            heroId
        );
        IERC20(glaTokenAddress).transfer(
            msg.sender,
            heroesStakingData[heroId].rewardEstimate
        );
        emit Withdraw(msg.sender, heroId, heroesStakingData[heroId].rewardEstimate);
        _removeHero(msg.sender, heroId);
        
    }
    
    function setMintOperator(address _mintOperator) public onlyOwner{
        mintOperator = _mintOperator;
    }
    
    function setGameManager(address _gameManager) public onlyOwner{
        gameManager = _gameManager;
    }

    function listHeroIdOf(address owner)
        public
        view
        returns (uint256[] memory)
    {
        uint256[] memory result = new uint256[](amountHeroStaking[owner]);
        for (uint256 i = 0; i < amountHeroStaking[msg.sender]; i++) {
            result[i] = listHero[owner][i];
        }
        return result;
    }

    function listHeroData(address owner)
        public
        view
        returns (HeroStakingData[] memory)
    {
        HeroStakingData[] memory result = new HeroStakingData[](
            amountHeroStaking[owner]
        );
        for (uint256 i = 0; i < amountHeroStaking[owner]; i++) {
            result[i] = heroesStakingData[listHero[owner][i]];
        }
        return result;
    }

    function heroStakingInfo(uint256 heroId)
        public
        view
        returns (
            address owner,
            uint256 plan,
            uint256 start,
            uint256 finish,
            uint256 reward
        )
    {
        owner = heroesStakingData[heroId].owner;
        plan = heroesStakingData[heroId].plan;
        start = heroesStakingData[heroId].start;
        finish = heroesStakingData[heroId].start.add(
            plans[plan].time.mul(STEP_TIME)
        );
        reward = heroesStakingData[heroId].rewardEstimate;
    }
    
    function emergencyWithdraw(uint256 amount) public onlyOwner{
        address glaHeroAddress = IGameManager(gameManager).getContract("GLAHeroNFT");
        address glaTokenAddress = IGameManager(gameManager).getContract("GLAToken");
        uint256 heroBalance = IGLAHeroNFT(glaHeroAddress).balanceOf(address(this));
        uint256[] memory heroIds = new uint256[](heroBalance);
        heroIds = IGLAHeroNFT(glaHeroAddress).getListHeroIdsOf(address(this));
        amount = amount > heroBalance ? heroBalance : amount;
        for(uint256 i; i < amount; i++){
            IGLAHeroNFT(glaHeroAddress).transferFrom(address(this), msg.sender, heroIds[i]);
        }
        uint256 glaBalance = IERC20(glaTokenAddress).balanceOf(address(this));
        if(glaBalance>0){
            IERC20(glaTokenAddress).transfer(msg.sender, glaBalance);
        }

    }

    function _removeHero(address owner, uint256 heroId) internal {
        uint256 lastTokenIndex = amountHeroStaking[owner] - 1;
        uint256 tokenIndex = stakedHeroIndex[heroId];

        // When the token to delete is the last token, the swap operation is unnecessary
        if (tokenIndex != lastTokenIndex) {
            uint256 lastTokenId = listHero[owner][lastTokenIndex];

            listHero[owner][tokenIndex] = lastTokenId; // Move the last token to the slot of the to-delete token
            stakedHeroIndex[lastTokenId] = tokenIndex; // Update the moved token's index
        }
        // This also deletes the contents at the last position of the array
        delete stakedHeroIndex[heroId];
        delete listHero[owner][lastTokenIndex];
        delete heroesStakingData[heroId];
        amountHeroStaking[owner] -= 1;
    }
}

interface IGLAHeroNFT is IERC721, IERC721Enumerable {
    function getHeroRarity(uint256 heroId_) external returns (uint8);
    function getListHeroIdsOf(address owner) external view returns(uint256[] memory);
}

interface IMintOperator {
    function mint(address account, uint256 amount) external;
}

interface IGameManager {
    function getContract(string memory contract_)
        external
        view
        returns (address);
}
