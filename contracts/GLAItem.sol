// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/proxy/utils/Initializable.sol";
contract GLAItem is ERC1155, Ownable, Initializable{
    using SafeMath for uint256;
    address public gameManager;
    uint256 private tokenId;
    mapping (string => uint256) private tokenIdOf; // item name => token id
    mapping (string => uint256) public priceOf; // item name => price

    event ChangeItemPrice(string itemType, uint256 price);
    event CreateNewItem(string newItem, uint256 price);


    constructor() ERC1155("GalaxyAdventure"){

    }
    modifier onlyGameManager {
        require(gameManager == msg.sender, "not authorized");
        _;
    }
   // Initialize Gem: 10 GLA; Core: 1000 GLA; Chest: 10000 GLA.
   // Other resources can be added later    
    function initialize(address gameManager_) public initializer {
        tokenIdOf["Gem"] = 1;
        priceOf["Gem"] = 1e19;
        tokenIdOf["Core"] = 2;
        priceOf["Core"] = 1e21;
        tokenIdOf["Chest"] = 3;
        priceOf["Chest"] = 1e22;
        tokenId = 3;
        gameManager = gameManager_;
    }
    // User buys item: buying fee transfered to dev wallet, item minted then transfered to user
    function buyItem(string memory itemType, uint256 amount) public {
        require(priceOf[itemType] > 0, "Item does not exist!");
        address devWallet = IGameManager(gameManager).getDevWallet();
        address tokenContract = IGameManager(gameManager).getContract("GLAToken");
        IGLAToken(tokenContract).transferFrom(msg.sender, devWallet, amount.mul(priceOf[itemType]));
        _mint(msg.sender, tokenIdOf[itemType], amount, "");
    }
    
    function getIdOf(string memory itemType) public view returns(uint256){
        return tokenIdOf[itemType];
    }
    
    // Called by GLAUpgrade, burn the item used to upgrade hero star
    function burn(address account, uint256 id, uint256 amount) public {
        require(account == _msgSender() || isApprovedForAll(account, _msgSender()), 
                "ERC1155: caller is not owner nor approved");
        _burn(account, id, amount);
    }
    
    function createNewItemType(string memory newTypeItem, uint256 price) public onlyGameManager {
        tokenId++;
        tokenIdOf[newTypeItem] = tokenId;
        priceOf[newTypeItem] = price;
        emit CreateNewItem(newTypeItem, price);
    }

    function isApprovedForAll(address account, address operator) public view virtual override returns (bool) {
        if(operator == IGameManager(gameManager).getContract("GLAUpgrade") ||
            operator == IGameManager(gameManager).getContract("GLASpawner"))
            return true;
        else
            return ERC1155.isApprovedForAll(account, operator);
    }
    
    function changeItemPrice(string memory itemType, uint256 price) public onlyGameManager {
        require(priceOf[itemType] > 0, "Item does not exist!");
        priceOf[itemType] = price;
        emit ChangeItemPrice(itemType, price);
    }
    
    function SetURI(string memory newURI) external onlyOwner {
        _setURI(newURI);
    }
    
    function setGameManager(address managerContract_) public onlyOwner {
        gameManager = managerContract_;
    }
}

interface IGLAToken{
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
}
    
interface IGameManager{
    function  getContract(string memory contract_) external view returns (address);
    function  getDevWallet() external view returns (address);
}
