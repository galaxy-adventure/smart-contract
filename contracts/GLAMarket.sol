// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/proxy/utils/Initializable.sol";
contract GLAMarket is Ownable, Initializable {
    using SafeMath for uint256;
    
    uint8 public marketFee;
    uint8 public maxFee;

    address public gameManager;

    mapping (uint256 => address) public heroOwners; // heroid to owner  
    mapping (uint256 => uint256) public heroPrices; // heroid to hero price
    
    // Mapping from owner to list of owned hero IDs
    mapping(address => mapping(uint256 => uint256)) private _ownedHeroes;
    // Mapping from hero ID to index of the owner heroes list
    mapping(uint256 => uint256) private _listedHeroesIndex;
    mapping(address => uint256) private heroesSalesOf;
    uint256 private totalNFTOnSale;

    event List(uint256 indexed heroId_, address owner_, uint256 price_);
    event Delist(uint256 indexed heroId_);
    event Purchase(address buyer_, uint256 indexed heroId_, uint256 price_);
    event ChangePrice(uint256 indexed heroId, uint256 newPrice);
    modifier only(string memory necessaryContract ) {
        require(IGameManager(gameManager).getContract(necessaryContract) == msg.sender, "not authorized");
        _;
    }

    modifier onlyGameManager {
        require(gameManager == msg.sender, "not authorized");
        _;
    }

    function initialize(address gameManager_) public initializer{
        gameManager = gameManager_; 
        marketFee = 5; // percent
        maxFee = 10;
    }

    // User has to `approve` to allow this contract to trasfer their NFTs
    function listOnMarket(uint256 heroId_, uint256 price_) public {
        address heroContract = IGameManager(gameManager).getContract("GLAHeroNFT");
        address owner_ = IGLAHeroNFT(heroContract).ownerOf(heroId_);
        require(msg.sender == owner_, "You not own this hero");
        IGLAHeroNFT(heroContract).transferFrom(owner_, address(this), heroId_);
        totalNFTOnSale+=1;
        heroOwners[heroId_] = owner_;
        heroPrices[heroId_] = price_;
        _ownedHeroes[owner_][heroesSalesOf[owner_]] = heroId_;
        _listedHeroesIndex[heroId_] = heroesSalesOf[owner_];
        heroesSalesOf[owner_]+=1;
        emit List(heroId_, owner_, price_);
    }

    // User can delist their NFT from the Market
    function delistAndWithdraw(uint256 heroId_) public {
        require(msg.sender == heroOwners[heroId_], "You are not the NFT owner!");
        _withdraw(heroId_);
        _delist(heroId_);
  }

    // Buy heroes of other users on the market
    function purchaseHero(uint256 heroId_) public {
        require(heroOwners[heroId_] != address(0), "This character not on sales");
        address GLATokenContract = IGameManager(gameManager).getContract("GLAToken");
        address devWallet = IGameManager(gameManager).getDevWallet();
        address heroContract = IGameManager(gameManager).getContract("GLAHeroNFT");
        
        uint256 realPrice = heroPrices[heroId_].mul(10**18); // Price in GLA
        uint256 fee = realPrice.mul(marketFee).div(100);
        uint256 ftReceive = realPrice.sub(fee); // Actual GLA received by the seller
        
        // Transfer GLA to the seller
        require(IGLAToken(GLATokenContract).transferFrom(msg.sender, heroOwners[heroId_], ftReceive) == true, "Purchase error");
        
        // Transfer transaction fee to dev wallet
        IGLAToken(GLATokenContract).transferFrom(msg.sender, devWallet, fee);
        emit Purchase(msg.sender, heroId_, heroPrices[heroId_]);
        _delist(heroId_);

        // Transfer NFT to the buyer
        IGLAHeroNFT(heroContract).transferFrom(address(this), msg.sender, heroId_);

    }
    
    function getAmountHeroOf(address owner) public view returns(uint256){
        return heroesSalesOf[owner];
    }
    
    function getTotalHeroOnSales() public view returns(uint256){
        return totalNFTOnSale;
    }
    
    function heroOfOwnerByIndex(address owner, uint256 index) public view  returns (uint256) {
        require(index < heroesSalesOf[owner], "owner index out of bounds");
        return _ownedHeroes[owner][index];
    }
    
    function getListHeroesOf(address owner) public view returns(HeroOnSales[] memory){
        address heroNFT = IGameManager(gameManager).getContract("GLAHeroNFT");
        uint256 totalHero = getAmountHeroOf(owner);
        HeroOnSales[] memory listHeroOnSales = new HeroOnSales[](totalHero);
        for (uint i=0; i<totalHero; i++){
            uint256 heroId_ = heroOfOwnerByIndex(owner, i);
            listHeroOnSales[i]=HeroOnSales(IGLAHeroNFT(heroNFT).getHero(heroId_), heroPrices[heroId_]);
        }
        return listHeroOnSales;
    }
    
    // Get list heroes to display on Frontend, 10 heroes/page
    // Page starts from 0
    function getHeroesPage(uint256 page_) public view returns(HeroOnSales[] memory){
        address heroNFT = IGameManager(gameManager).getContract("GLAHeroNFT");
        Hero[] memory listAllHeroesMarket = IGLAHeroNFT(heroNFT).getListHeroesOf(address(this));
        require(listAllHeroesMarket.length > page_*10 ,"Out of bounds");
        uint256 stop = listAllHeroesMarket.length - 10 * page_;
        uint256 start = 0;
        uint256 count =0;
        if (listAllHeroesMarket.length > (page_+1)*10)
            start = listAllHeroesMarket.length - (page_+1)*10;
        HeroOnSales[] memory heroesPage = new HeroOnSales[](stop - start);
        for(uint256 i = start; i<stop; i++){
            heroesPage[count] = HeroOnSales(listAllHeroesMarket[i], heroPrices[listAllHeroesMarket[i].heroId]);
            count++;
        }
        return heroesPage;
    }
    
    function getHeroPrice(uint256 heroId_) public view returns(uint256){
        return heroPrices[heroId_];
    }
    
    function setHeroPrice(uint256 heroId_, uint256 price_) public {
        require(msg.sender == heroOwners[heroId_], "You are not the NFT owner!");
        heroPrices[heroId_] = price_;
        emit ChangePrice(heroId_, price_);
    }
    
    function _delist(uint256 heroId_) internal {
        totalNFTOnSale--;
        address currentOwner = heroOwners[heroId_];
        heroOwners[heroId_] = address(0);
        heroPrices[heroId_] = 0;
        // To prevent a gap in from's tokens array, we store the last token in the index of the token to delete, and
        // then delete the last slot (swap and pop).

        uint256 lastHeroIndex = heroesSalesOf[currentOwner] - 1;
        heroesSalesOf[currentOwner]-=1;
        uint256 heroIndex = _listedHeroesIndex[heroId_];

        // When the token to be deleted is the last token, the swap operation is unnecessary
        if (heroIndex != lastHeroIndex) {
            uint256 lastHeroId = _ownedHeroes[currentOwner][lastHeroIndex];

            _ownedHeroes[currentOwner][heroIndex] = lastHeroId; // Move the last token to the slot of the to-delete token
            _listedHeroesIndex[lastHeroId] = heroIndex; // Update the moved token's index
        }
        // This also deletes the contents at the last position of the array
        delete _listedHeroesIndex[heroId_];
        delete _ownedHeroes[currentOwner][lastHeroIndex];
        
        emit Delist(heroId_);
    }

    function _withdraw(uint256 heroId_) internal {
        address heroContract = IGameManager(gameManager).getContract("GLAHeroNFT");
        IGLAHeroNFT(heroContract).transferFrom(address(this), heroOwners[heroId_], heroId_);
    }
  
    function setFee(uint8 fee) external onlyGameManager {
        if (fee > maxFee){
          marketFee = maxFee;
        } else {
          marketFee = fee;
        }
    }

    function setManagerContract(address manager) public onlyOwner {
        gameManager = manager;
    }

}

interface IGameManager{
    function  getContract(string memory contract_) external view returns (address);
    function  getDevWallet() external view returns (address);
}

interface IGLAHeroNFT{
    function mint (address owner, uint heroType, uint8 rarity) external;
    function transferFrom(address from, address to, uint256 tokenId) external;
    function ownerOf(uint256 tokenId) external view returns (address);
    function getHero(uint256 heroId_) external view returns(Hero memory);
    function getListHeroesOf(address owner) external view returns(Hero[] memory);
    function getListHeroIdsOf(address owner) external view returns(uint256[] memory);
}

interface IGLAToken{
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
}

    struct Hero {
        uint256 heroId;
        uint types;
        string name;
        uint8 rarity;
        uint8 level;
        uint256 experience;    
        uint256 lastBattleTime;
    }
    
    struct HeroOnSales{
    Hero hero;
    uint256 price;
    }
