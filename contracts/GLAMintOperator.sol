// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;
import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";

contract MintOperator is AccessControlEnumerable{
    address gameManager;
    bytes32 constant public MINTER_ROLE = keccak256("MINTER_ROLE");
    constructor(address _gameManager){
        gameManager =_gameManager;
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }
    
    function mint(address to, uint256 amount) public onlyRole(MINTER_ROLE){
        address glaTokenContract = IGameManager(gameManager).getContract("GLAToken");
        IGLAToken(glaTokenContract).mint(to, amount);
    }
    
    function setGameManager(address _gameManager) public onlyRole(DEFAULT_ADMIN_ROLE){
        gameManager = _gameManager;
    }
}

interface IGLAToken {
    function mint(address to, uint256 amount) external;
}

interface IGameManager {
    function getContract(string memory contract_)
        external
        view
        returns (address);
}
