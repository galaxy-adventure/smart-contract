//SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;
import "@openzeppelin/contracts/proxy/transparent/TransparentUpgradeableProxy.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/proxy/transparent/ProxyAdmin.sol";

contract GLAProxyAdmin is ProxyAdmin{}

contract ProxyHeroNFT is TransparentUpgradeableProxy, Ownable{
    constructor(address logic, address admin, bytes memory data) TransparentUpgradeableProxy(logic, admin, data) {
    }
}

contract ProxyItem is TransparentUpgradeableProxy, Ownable{
    constructor(address logic, address admin, bytes memory data) TransparentUpgradeableProxy(logic, admin, data) {
    }
}

contract ProxyMarket is TransparentUpgradeableProxy, Ownable{
    constructor(address logic, address admin, bytes memory data) TransparentUpgradeableProxy(logic, admin, data) {
    }
}

