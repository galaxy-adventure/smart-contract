// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

contract GLASeedSale is Ownable {
    struct SeedsaleInfo {
        uint256 depositBalance;
        uint256 totalClaimable;
        uint256 claimed;
    }
    using SafeMath for uint256;
    
    address teamWallet;
    address glaTokenAddress;
    
    mapping(address=>bool) seedAddresses; // Addresses approved in Seed sale
    mapping (address=>SeedsaleInfo) seedsaleInfos;
    mapping(uint256 => uint256) unlockFundRate; // phase => Unlock rate
       
    //Seedsale price: 0.0007 BUSD = 1 GLA
    uint256 rate = 7;
    // 10 minutes after PancakeSwap listing, seedsalers can claim part of their tokens
    uint256 tgeUnlockTime;
    // Unlock partly every 30 days
    uint256 unlockTimeLock = 30 days;

    bool public isUnlockClaim = false; // Allow to claim
    
    constructor(address _glaTokenAddress, address _teamWallet){
        glaTokenAddress = _glaTokenAddress;
        teamWallet = _teamWallet;
        // unlock 10% at TGE, 15% every month after TGE
        unlockFundRate[0] = 10; 
        unlockFundRate[1] = 25;
        unlockFundRate[2] = 40;
        unlockFundRate[3] = 55;
        unlockFundRate[4] = 70;
        unlockFundRate[5] = 85;
        unlockFundRate[6] = 100;
        
        registSeedsaleAddress();
    }
    
    // if cannot sell all public pool, withdraw the remaining GLA token to team Wallet
    function withdrawPoolAmount() public onlyOwner{
        IERC20(glaTokenAddress).transfer(teamWallet, IERC20(glaTokenAddress).balanceOf(address(this)));
    }
    
    function unlockClaim() public onlyOwner{
        tgeUnlockTime = block.timestamp;
        isUnlockClaim = true;
    }
    
    function setGLAToken(address _glaTokenAddress) public onlyOwner{
        glaTokenAddress = _glaTokenAddress;
    }
    
    function setTeamWallet(address _teamWallet) public onlyOwner{
        teamWallet = _teamWallet;
    }
    
    // seedsaler can change their address to receive GLA
    function changeReceiver(address _changeAddress) public{
        //verify seedsale address
        require(isSeedsaleAddress(_msgSender()),"Not in seed sale address");
        
        // delete old address
        seedAddresses[_msgSender()] = false;
        
        // add replaced address
        seedAddresses[_changeAddress] = true;
        
        // move info from old to new address
        seedsaleInfos[_changeAddress] = seedsaleInfos[_msgSender()]; 
        
    }

    function registSeedsaleAddress() internal {
        seedAddresses[address(0x2720e230415bA18B22afdf1912B2BbA3D163e74F)] = true ;
        seedAddresses[address(0x55190075b6258da3806DdfCB24034f188b4a4308)] = true ;
        seedAddresses[address(0x95c756b08d8E564bb999978229e3E7d69A198e85)] = true ;
        seedAddresses[address(0xE20012F93039a06694aF53a302C786608630608f)] = true ;
        seedAddresses[address(0x9b80098976F835eAAe5Bf5DE6632Dd0e0ae6BC4b)] = true ;
        seedAddresses[address(0x402E9F43be4559f6488aD19CDF4E08e421063A2c)] = true ;
        seedAddresses[address(0xdE1Db4EF1cA0ed75716597148363A956C1CD87e3)] = true ;
        seedAddresses[address(0xB96328AE7696B65EA693f4d2F64BcF003F1B98a2)] = true ;
        seedAddresses[address(0xB4a8e5f28EEa7D5C54A6aDfdCf2815a84f6e690e)] = true ;
        seedAddresses[address(0x4DE7B40CEa9eF3Cd960B79B4152ae3206AB36B2A)] = true ;
        seedAddresses[address(0x4c4C589949269c81B7bE02A7A0F996cAbEe65d83)] = true ;
        seedAddresses[address(0xF9653a83407bdd4909f0e4e77038F3F7cCfc41E1)] = true ;
        seedAddresses[address(0xe91E4EF88Bf8dfc606Bd3713Af55C139E9ad8723)] = true ;
        seedAddresses[address(0x53C85ce5a2C23f44f826a9082DB669e1e52b5cfC)] = true ;
        seedAddresses[address(0xFaE8e95D1bc15eDD99177d7bB6435664f10B3060)] = true ;
        seedAddresses[address(0xFa8f3012c8f4C140DeAb611823813c66b8eB8b76)] = true ;
        seedAddresses[address(0xe631BA8E82ce76B68960932154d3b974942cb0F8)] = true ;
        seedAddresses[address(0xE4Bc1B91d0Fcea777FF8731c32f5ef384cc0822c)] = true ;
        seedAddresses[address(0xAf7BF57e7235D0ab285252D72667094Fb14D9a1b)] = true ;
        seedAddresses[address(0xCff51d72427d8a6e8c87fe020Fe57Ffc5b811696)] = true ;
        seedAddresses[address(0xbD5519d4eb8f4eDf45B14F354BbcaEFdb4CA8bdD)] = true ;
        seedAddresses[address(0xb4E3603244657b2f95B037f1E8e924c7F778a555)] = true ;
        seedAddresses[address(0xb15EF4E7990d3F917A6aCF373aA4BdF99505c987)] = true ;
        seedAddresses[address(0xb0404CF31e87031984C851576d2bA242596395aB)] = true ;
        seedAddresses[address(0xa57aEBB29f5b099b395DDa7d4aDD329BFC6BECE0)] = true ;
        seedAddresses[address(0xa09F4c7BD7E678f04DB7b7D5aa6fE217894cc74E)] = true ;
        seedAddresses[address(0x820792f6babdb65F8BcCB1fC867BDb5e9ae47042)] = true ;
        seedAddresses[address(0x51F31F9CD43b5E3014c2fd748012E46e337B44F0)] = true ;
        seedAddresses[address(0x4FFA7906b2C5d132b081496A4742AdEF48B62588)] = true ;
        seedAddresses[address(0x44A6aE9c0B1D514585EC2C109872f6d7020278BE)] = true ;
        seedAddresses[address(0x10099b1440faC996A11907f9ed5F8FD65b52bD95)] = true ;
        seedAddresses[address(0x270d1cb17D359bf3d9138EaFBd78714Da90bb53a)] = true ;
        seedAddresses[address(0x21d848E38f57531cECE8F5507a8AccD11055C117)] = true ;
        seedAddresses[address(0xdcB25E4CC58840AD34191c47b59Ce497E28111D2)] = true ;
        seedAddresses[address(0xdc8304DDCB306d66Ff0B64783741F85b1826a657)] = true ;
        seedAddresses[address(0xD4FA455B18783c39C964912eA3266be79f8298bd)] = true ;
        seedAddresses[address(0xCacEdbC508AaFF1508E3C39DBD91eB3D9a5056a1)] = true ;
        seedAddresses[address(0xc632594613DF98caA0f8Ce63695B67346dFcE06e)] = true ;
        seedAddresses[address(0xbD93171Cc8BcE4F73D8B3744d51a6fc3AC474D05)] = true ;
        seedAddresses[address(0xaf5Df7871816cA1a962E00549A3302e37705ce5a)] = true ;
        seedAddresses[address(0x91BCeaDC08d291087F82f95741aCb342D1fC03cE)] = true ;
        seedAddresses[address(0x72D76B08fCdd9d24A93C5015d829801d299a764a)] = true ;
        seedAddresses[address(0x596aD0fEA3c297BB7f68f234A7D5B355b9207B72)] = true ;
        seedAddresses[address(0x57d52749bcea876dB730043E52E2F74F98bf4C2C)] = true ;
        seedAddresses[address(0x47Fc5bede974F5799745Baba1B173464b6948853)] = true ;
        seedAddresses[address(0x3a30d837e02b378e416FAbd80335742799f25767)] = true ;
        seedAddresses[address(0x395d21D5b1C5737DfbF072eB6939Afd7683A6166)] = true ;
        seedAddresses[address(0x2cC93540C55dE7d3B2B03e568B21FF4cc21B7161)] = true ;
        seedAddresses[address(0x246A758873A76efBd260620f6807022d6f8f5281)] = true ;
        seedAddresses[address(0x1b3ea5C4dc787FB3F9401f9a288bd290867cf2D1)] = true ;
        seedAddresses[address(0x1A93d9B8a2b9976EF1C21810f2D76df7d7FE7cAc)] = true ;
        seedAddresses[address(0x182f4e92AC3d1629b501cc02d0F6141966C80476)] = true ;
        seedAddresses[address(0x07868D7B0B52C92F8d46E78D3AB72A09D4CFD0d7)] = true ;
        seedAddresses[address(0x017658833a321c8614fb0D3e9d768884D032f0Db)] = true ;
        seedAddresses[address(0xACF1938387b45bCF2402fab5BDe12089f73370d7)] = true ;
        seedAddresses[address(0x5Cf54C4a2951A54b70499A8F3943fe69a74b0e40)] = true ;
        seedAddresses[address(0x6ca00dF0D83Acb35b01d175ABcDEFC043230a121)] = true ;
        seedAddresses[address(0xb297A3bC6AE780952B927e77c86A17c58A4C6eB9)] = true ;
        seedAddresses[address(0x8d53C491c3e874AF068FD81304246b34464D550e)] = true ;

        seedsaleInfos[address(0x2720e230415bA18B22afdf1912B2BbA3D163e74F)].depositBalance = 1950*10**18;
        seedsaleInfos[address(0x55190075b6258da3806DdfCB24034f188b4a4308)].depositBalance = 1150*10**18;
        seedsaleInfos[address(0x95c756b08d8E564bb999978229e3E7d69A198e85)].depositBalance = 905*10**18;
        seedsaleInfos[address(0xE20012F93039a06694aF53a302C786608630608f)].depositBalance = 860*10**18;
        seedsaleInfos[address(0x9b80098976F835eAAe5Bf5DE6632Dd0e0ae6BC4b)].depositBalance = 850*10**18;
        seedsaleInfos[address(0x402E9F43be4559f6488aD19CDF4E08e421063A2c)].depositBalance = 825*10**18;
        seedsaleInfos[address(0xdE1Db4EF1cA0ed75716597148363A956C1CD87e3)].depositBalance = 775*10**18;
        seedsaleInfos[address(0xB96328AE7696B65EA693f4d2F64BcF003F1B98a2)].depositBalance = 750*10**18;
        seedsaleInfos[address(0xB4a8e5f28EEa7D5C54A6aDfdCf2815a84f6e690e)].depositBalance = 750*10**18;
        seedsaleInfos[address(0x4DE7B40CEa9eF3Cd960B79B4152ae3206AB36B2A)].depositBalance = 625*10**18;
        seedsaleInfos[address(0x4c4C589949269c81B7bE02A7A0F996cAbEe65d83)].depositBalance = 625*10**18;
        seedsaleInfos[address(0xF9653a83407bdd4909f0e4e77038F3F7cCfc41E1)].depositBalance = 605*10**18;
        seedsaleInfos[address(0xe91E4EF88Bf8dfc606Bd3713Af55C139E9ad8723)].depositBalance = 605*10**18;
        seedsaleInfos[address(0x53C85ce5a2C23f44f826a9082DB669e1e52b5cfC)].depositBalance = 525*10**18;
        seedsaleInfos[address(0xFaE8e95D1bc15eDD99177d7bB6435664f10B3060)].depositBalance = 425*10**18;
        seedsaleInfos[address(0xFa8f3012c8f4C140DeAb611823813c66b8eB8b76)].depositBalance = 425*10**18;
        seedsaleInfos[address(0xe631BA8E82ce76B68960932154d3b974942cb0F8)].depositBalance = 425*10**18;
        seedsaleInfos[address(0xE4Bc1B91d0Fcea777FF8731c32f5ef384cc0822c)].depositBalance = 425*10**18;
        seedsaleInfos[address(0xAf7BF57e7235D0ab285252D72667094Fb14D9a1b)].depositBalance = 425*10**18;
        seedsaleInfos[address(0xCff51d72427d8a6e8c87fe020Fe57Ffc5b811696)].depositBalance = 425*10**18;
        seedsaleInfos[address(0xbD5519d4eb8f4eDf45B14F354BbcaEFdb4CA8bdD)].depositBalance = 425*10**18;
        seedsaleInfos[address(0xb4E3603244657b2f95B037f1E8e924c7F778a555)].depositBalance = 425*10**18;
        seedsaleInfos[address(0xb15EF4E7990d3F917A6aCF373aA4BdF99505c987)].depositBalance = 425*10**18;
        seedsaleInfos[address(0xb0404CF31e87031984C851576d2bA242596395aB)].depositBalance = 425*10**18;
        seedsaleInfos[address(0xa57aEBB29f5b099b395DDa7d4aDD329BFC6BECE0)].depositBalance = 425*10**18;
        seedsaleInfos[address(0xa09F4c7BD7E678f04DB7b7D5aa6fE217894cc74E)].depositBalance = 425*10**18;
        seedsaleInfos[address(0x820792f6babdb65F8BcCB1fC867BDb5e9ae47042)].depositBalance = 425*10**18;
        seedsaleInfos[address(0x51F31F9CD43b5E3014c2fd748012E46e337B44F0)].depositBalance = 425*10**18;
        seedsaleInfos[address(0x4FFA7906b2C5d132b081496A4742AdEF48B62588)].depositBalance = 425*10**18;
        seedsaleInfos[address(0x44A6aE9c0B1D514585EC2C109872f6d7020278BE)].depositBalance = 425*10**18;
        seedsaleInfos[address(0x10099b1440faC996A11907f9ed5F8FD65b52bD95)].depositBalance = 425*10**18;
        seedsaleInfos[address(0x270d1cb17D359bf3d9138EaFBd78714Da90bb53a)].depositBalance = 425*10**18;
        seedsaleInfos[address(0x21d848E38f57531cECE8F5507a8AccD11055C117)].depositBalance = 425*10**18;
        seedsaleInfos[address(0xdcB25E4CC58840AD34191c47b59Ce497E28111D2)].depositBalance = 350*10**18;
        seedsaleInfos[address(0xdc8304DDCB306d66Ff0B64783741F85b1826a657)].depositBalance = 350*10**18;
        seedsaleInfos[address(0xD4FA455B18783c39C964912eA3266be79f8298bd)].depositBalance = 350*10**18;
        seedsaleInfos[address(0xCacEdbC508AaFF1508E3C39DBD91eB3D9a5056a1)].depositBalance = 350*10**18;
        seedsaleInfos[address(0xc632594613DF98caA0f8Ce63695B67346dFcE06e)].depositBalance = 350*10**18;
        seedsaleInfos[address(0xbD93171Cc8BcE4F73D8B3744d51a6fc3AC474D05)].depositBalance = 350*10**18;
        seedsaleInfos[address(0xaf5Df7871816cA1a962E00549A3302e37705ce5a)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x91BCeaDC08d291087F82f95741aCb342D1fC03cE)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x72D76B08fCdd9d24A93C5015d829801d299a764a)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x596aD0fEA3c297BB7f68f234A7D5B355b9207B72)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x57d52749bcea876dB730043E52E2F74F98bf4C2C)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x47Fc5bede974F5799745Baba1B173464b6948853)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x3a30d837e02b378e416FAbd80335742799f25767)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x395d21D5b1C5737DfbF072eB6939Afd7683A6166)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x2cC93540C55dE7d3B2B03e568B21FF4cc21B7161)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x246A758873A76efBd260620f6807022d6f8f5281)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x1b3ea5C4dc787FB3F9401f9a288bd290867cf2D1)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x1A93d9B8a2b9976EF1C21810f2D76df7d7FE7cAc)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x182f4e92AC3d1629b501cc02d0F6141966C80476)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x07868D7B0B52C92F8d46E78D3AB72A09D4CFD0d7)].depositBalance = 350*10**18;
        seedsaleInfos[address(0x017658833a321c8614fb0D3e9d768884D032f0Db)].depositBalance = 375*10**18;
        seedsaleInfos[address(0xACF1938387b45bCF2402fab5BDe12089f73370d7)].depositBalance = 200*10**18;
        seedsaleInfos[address(0x5Cf54C4a2951A54b70499A8F3943fe69a74b0e40)].depositBalance = 200*10**18;
        seedsaleInfos[address(0x6ca00dF0D83Acb35b01d175ABcDEFC043230a121)].depositBalance = 150*10**18;
        seedsaleInfos[address(0xb297A3bC6AE780952B927e77c86A17c58A4C6eB9)].depositBalance = 100*10**18;
        seedsaleInfos[address(0x8d53C491c3e874AF068FD81304246b34464D550e)].depositBalance = 100*10**18;
        
        seedsaleInfos[address(0x2720e230415bA18B22afdf1912B2BbA3D163e74F)].totalClaimable = uint256(1950*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x55190075b6258da3806DdfCB24034f188b4a4308)].totalClaimable = uint256(1150*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x95c756b08d8E564bb999978229e3E7d69A198e85)].totalClaimable = uint256(905*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xE20012F93039a06694aF53a302C786608630608f)].totalClaimable = uint256(860*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x9b80098976F835eAAe5Bf5DE6632Dd0e0ae6BC4b)].totalClaimable = uint256(850*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x402E9F43be4559f6488aD19CDF4E08e421063A2c)].totalClaimable = uint256(825*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xdE1Db4EF1cA0ed75716597148363A956C1CD87e3)].totalClaimable = uint256(775*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xB96328AE7696B65EA693f4d2F64BcF003F1B98a2)].totalClaimable = uint256(750*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xB4a8e5f28EEa7D5C54A6aDfdCf2815a84f6e690e)].totalClaimable = uint256(750*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x4DE7B40CEa9eF3Cd960B79B4152ae3206AB36B2A)].totalClaimable = uint256(625*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x4c4C589949269c81B7bE02A7A0F996cAbEe65d83)].totalClaimable = uint256(625*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xF9653a83407bdd4909f0e4e77038F3F7cCfc41E1)].totalClaimable = uint256(605*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xe91E4EF88Bf8dfc606Bd3713Af55C139E9ad8723)].totalClaimable = uint256(605*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x53C85ce5a2C23f44f826a9082DB669e1e52b5cfC)].totalClaimable = uint256(525*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xFaE8e95D1bc15eDD99177d7bB6435664f10B3060)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xFa8f3012c8f4C140DeAb611823813c66b8eB8b76)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xe631BA8E82ce76B68960932154d3b974942cb0F8)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xE4Bc1B91d0Fcea777FF8731c32f5ef384cc0822c)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xAf7BF57e7235D0ab285252D72667094Fb14D9a1b)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xCff51d72427d8a6e8c87fe020Fe57Ffc5b811696)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xbD5519d4eb8f4eDf45B14F354BbcaEFdb4CA8bdD)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xb4E3603244657b2f95B037f1E8e924c7F778a555)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xb15EF4E7990d3F917A6aCF373aA4BdF99505c987)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xb0404CF31e87031984C851576d2bA242596395aB)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xa57aEBB29f5b099b395DDa7d4aDD329BFC6BECE0)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xa09F4c7BD7E678f04DB7b7D5aa6fE217894cc74E)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x820792f6babdb65F8BcCB1fC867BDb5e9ae47042)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x51F31F9CD43b5E3014c2fd748012E46e337B44F0)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x4FFA7906b2C5d132b081496A4742AdEF48B62588)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x44A6aE9c0B1D514585EC2C109872f6d7020278BE)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x10099b1440faC996A11907f9ed5F8FD65b52bD95)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x270d1cb17D359bf3d9138EaFBd78714Da90bb53a)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x21d848E38f57531cECE8F5507a8AccD11055C117)].totalClaimable = uint256(425*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xdcB25E4CC58840AD34191c47b59Ce497E28111D2)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xdc8304DDCB306d66Ff0B64783741F85b1826a657)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xD4FA455B18783c39C964912eA3266be79f8298bd)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xCacEdbC508AaFF1508E3C39DBD91eB3D9a5056a1)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xc632594613DF98caA0f8Ce63695B67346dFcE06e)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xbD93171Cc8BcE4F73D8B3744d51a6fc3AC474D05)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xaf5Df7871816cA1a962E00549A3302e37705ce5a)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x91BCeaDC08d291087F82f95741aCb342D1fC03cE)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x72D76B08fCdd9d24A93C5015d829801d299a764a)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x596aD0fEA3c297BB7f68f234A7D5B355b9207B72)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x57d52749bcea876dB730043E52E2F74F98bf4C2C)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x47Fc5bede974F5799745Baba1B173464b6948853)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x3a30d837e02b378e416FAbd80335742799f25767)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x395d21D5b1C5737DfbF072eB6939Afd7683A6166)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x2cC93540C55dE7d3B2B03e568B21FF4cc21B7161)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x246A758873A76efBd260620f6807022d6f8f5281)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x1b3ea5C4dc787FB3F9401f9a288bd290867cf2D1)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x1A93d9B8a2b9976EF1C21810f2D76df7d7FE7cAc)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x182f4e92AC3d1629b501cc02d0F6141966C80476)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x07868D7B0B52C92F8d46E78D3AB72A09D4CFD0d7)].totalClaimable = uint256(350*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x017658833a321c8614fb0D3e9d768884D032f0Db)].totalClaimable = uint256(375*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xACF1938387b45bCF2402fab5BDe12089f73370d7)].totalClaimable = uint256(200*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x5Cf54C4a2951A54b70499A8F3943fe69a74b0e40)].totalClaimable = uint256(200*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x6ca00dF0D83Acb35b01d175ABcDEFC043230a121)].totalClaimable = uint256(150*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0xb297A3bC6AE780952B927e77c86A17c58A4C6eB9)].totalClaimable = uint256(100*10**18).mul(10000).div(rate);
        seedsaleInfos[address(0x8d53C491c3e874AF068FD81304246b34464D550e)].totalClaimable = uint256(100*10**18).mul(10000).div(rate);
        
    }
    
    function claim() public {
        require(isUnlockClaim, "Not unlock claim");
        require(isSeedsaleAddress(_msgSender()),"Not in seed sale address");

        uint256 unlockedAmount = getUnlockedAmount(_msgSender());

        uint256 availableForClaimAmount = availableClaim(_msgSender());
        require(availableForClaimAmount > 0, "Nothing for claim");
        
        IERC20(glaTokenAddress).transfer(_msgSender(), availableForClaimAmount);
        //set the amount claimed by seedsaler
        seedsaleInfos[_msgSender()].claimed = unlockedAmount;
    }
    
    function getTotalDeposit(address _address) public view returns(uint256){
        return seedsaleInfos[_address].depositBalance;
    }
    
    function getTotalClaimableBalance(address _address) public view returns(uint256){
        return seedsaleInfos[_address].totalClaimable;
    }
    
    function getNextTimestampClaim() public view returns(uint256){
        uint256 unlockFundRateIndex = block.timestamp.sub(tgeUnlockTime).div(unlockTimeLock).add(1);
        return unlockFundRateIndex.mul(unlockTimeLock).add(tgeUnlockTime);
    }
    
    function isSeedsaleAddress(address _address) public view returns(bool){
        return seedAddresses[_address];
    }
    
    function availableRemainClaimable(address _address) public view returns(uint256){
        return  seedsaleInfos[_address].totalClaimable.sub(seedsaleInfos[_address].claimed);
    }
    
    function availableClaim(address _address) public view returns(uint256){
        uint256 unlockedAmount = getUnlockedAmount(_address);
        return unlockedAmount - seedsaleInfos[_address].claimed;
    }
    
    function getUnlockedAmount(address _address) internal view returns(uint256){
        // get index of unlockFundRate
        uint256 unlockFundRateIndex = block.timestamp.sub(tgeUnlockTime).div(unlockTimeLock);

        if(unlockFundRateIndex > 6) 
            unlockFundRateIndex = 6;
        // return rate * claimable tokens
        return seedsaleInfos[_address].totalClaimable.mul(unlockFundRate[unlockFundRateIndex]).div(100);
    }
}

