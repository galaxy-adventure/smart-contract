// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/access/Ownable.sol";


contract GLASpawner is Ownable{
    uint private rarity1 = 7500; // threshold for 1-star hero  
    uint private rarity2 = 9375; // threshold for 2-star hero
    
    uint256 newHeroPrice = 7e21;
    
    address private gameManager;
    
    event OpenChest(address indexed user, uint heroType, uint8 rarity);
    event BuyNewHero(address indexed user, uint256 heroType);
    event SetPrice(uint256 newHeroPrice);

    modifier onlyGameManager {
        require(gameManager == msg.sender, "not authorized");
        _;
    }

    constructor(address gameManager_) Ownable() {
        setGameManager(gameManager_);
    }
    
    // user buys 1-star hero
    function buyNewHero(uint heroType) public {
        require(heroType < 3, "Unavailable type hero");
        require(tx.origin == msg.sender, "don't try to cheat");

        address devWallet = IGameManager(gameManager).getDevWallet();
        address GLATokenContract = IGameManager(gameManager).getContract("GLAToken");
    
        IGLAToken(GLATokenContract).transferFrom(msg.sender, devWallet, newHeroPrice);
        _mint(msg.sender, heroType, 1);
        emit BuyNewHero(msg.sender, heroType);
    }
    
    function openERC1155Chest() public {
        require(tx.origin == msg.sender, "don't try to cheat");
        address glaItem = IGameManager(gameManager).getContract("GLAItem");
        IGLAItem(glaItem).burn(msg.sender, 3, 1);
        uint256 rnd = _random(10000);
        uint8 rarity;
        if (rnd < rarity1) 
            rarity = 1;
        else if (rnd < rarity2)
            rarity = 2;
        else
            rarity = 3;

        uint heroType = _random(3);

         _mint(msg.sender, heroType, rarity);
        emit OpenChest(msg.sender, heroType, rarity);
    }

    function setPrice(uint256 newHeroPrice_) external onlyGameManager  {
        newHeroPrice = newHeroPrice_;
        emit SetPrice(newHeroPrice_);
    }
    
    function getHeroPrice() public view returns(uint256) {
        return newHeroPrice;
    }
    
    function _random(uint256 range) internal view returns (uint256)  {
        return uint256(keccak256(abi.encodePacked(
                                                    msg.sender, 
                                                    blockhash(block.number-1), 
                                                    block.gaslimit, 
                                                    block.timestamp, 
                                                    gasleft())))%range;      
    }

    function _mint(address owner, uint heroType, uint8 rarity) internal{
        address heroContract = IGameManager(gameManager).getContract("GLAHeroNFT");
        IGLAHeroNFT(heroContract).mint(owner, heroType, rarity);
    }

    function setGameManager(address managerContract_) public onlyOwner {
        gameManager = managerContract_;
    }
}

interface IGameManager{
    function getContract(string memory contract_) external view returns (address);
    function getDevWallet() external view returns (address);
}

interface IGLAHeroNFT{
    function mint (address owner, uint heroType, uint8 rarity) external;
}

interface IGLAToken{
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
}

interface IGLAItem{
    function getIdOf(string memory itemType) external view returns(uint256);
    function burn(address account, uint256 id, uint256 amount) external;
}
