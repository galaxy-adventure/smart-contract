//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract GLAStaking is Ownable {
    using SafeMath for uint256;
    struct Deposit {
        uint256 index;
        uint8 plan;
        uint256 amount;
        uint256 start;
    }

    struct User {
        mapping(uint256 => Deposit) deposits;
        uint256[] depositIdx;
        uint256 numberOfDeposits;
    }

    struct Plan {
        uint256 time;
        uint256 percent; //APY
        uint256 maxStake;
        uint256 currentStake;
    }

    struct DepositInfo {
        uint256 depositIndex;
        uint256 amountDeposit;
        uint8 plan;
        uint256 percent;
        uint256 amountGain;
        uint256 start;
        uint256 finish;
    }
    
    uint256 depositID = 0;
    uint256 constant TIME_STEP = 1 days;
    uint256 public constant MIN_STAKE_AMOUNT = 10**4 * 10**18;
    address public glaMinter;
    address public gameManager;
    uint256 public totalStaked;
    mapping(uint256 => Plan) internal plans;
    mapping(address => User) internal users;

    event NewDeposit(address indexed user, uint256 idx, uint8 plan, uint256 amount);
    event UnstakeBeforeMaturity( address indexed user, uint256 idx, uint256 withdrawalAmount, uint256 time);
    event UnstakeAfterMaturity(address indexed user, uint256 idx, uint256 withdrawalAmount, uint256 time);

    constructor(address _gameManager, address _minter) {
        plans[0] = Plan(30, 180, 50 * 10**6 * 10**18, 0);
        plans[1] = Plan(60, 240, 105 * 10**6 * 10**18, 0);
        plans[2] = Plan(90, 300, 155 * 10**6 * 10**18, 0);
        glaMinter = _minter;
        gameManager = _gameManager;
    }

    function stakeGLA(uint256 _amount, uint8 plan) public {
        require(msg.sender == tx.origin, "Hello, bot!");
        require(plan < 3, "Invalid plan");
        require(
            _amount >= MIN_STAKE_AMOUNT,
            "Stake amount must be greater than MIN_STAKE_AMOUNT"
        );
        require(
            plans[plan].currentStake + _amount < plans[plan].maxStake,
            "This pool is full"
        );
        address glaTokenAddress = IGameManager(gameManager).getContract(
            "GLAToken"
        );
        
        User storage user = users[msg.sender];
        IGLAToken(glaTokenAddress).transferFrom(
            msg.sender,
            address(this),
            _amount
        );
        
        IGLAMintOperator(glaMinter).mint(
            address(this),
            _amount.mul(plans[plan].percent).mul(plans[plan].time).div(100).div(365)
        );
        
        uint256 idx = depositID ;
        user.deposits[idx] = Deposit(
            idx,
            plan,
            _amount,
            block.timestamp
        );
        
        totalStaked = totalStaked.add(_amount);
        plans[plan].currentStake += _amount;
        user.numberOfDeposits += 1;
        user.depositIdx.push(idx);
        depositID += 1;
        emit NewDeposit(msg.sender, idx, plan, _amount);
    }
    
    function unstakeGLA(uint256 idx) public {
        bool mature = _afterMaturity(idx);
        
        if (mature){
            _unstakeAfterMaturity(idx);
        }
        else{
            _unstakeBeforeMaturity(idx);
        }
    }

    function _unstakeAfterMaturity(uint256 idx) internal {

        User storage user = users[msg.sender];
        Plan memory plan = plans[user.deposits[idx].plan];
        
        uint256 principal = user.deposits[idx].amount;
        uint256 reward = principal.mul(plan.percent)
                                    .mul(plan.time)
                                    .div(100)
                                    .div(365);

        uint256 withdrawalAmount = principal.add(reward); 

        totalStaked -= principal;
        plans[user.deposits[idx].plan].currentStake -= principal;
        delete users[msg.sender].deposits[idx];
        user.depositIdx = remove(idx, user.depositIdx);
        user.numberOfDeposits -= 1;

        require(withdrawalAmount > 0, "Nothing to unstake");

        address glaTokenAddress = IGameManager(gameManager).getContract("GLAToken");
        IGLAToken(glaTokenAddress).transfer(
            msg.sender,
            withdrawalAmount
        );

        emit UnstakeAfterMaturity(msg.sender, idx, withdrawalAmount, block.timestamp);
    }

    function _unstakeBeforeMaturity(uint256 idx) internal {
        User storage user = users[msg.sender];
        address glaTokenAddress = IGameManager(gameManager).getContract("GLAToken");
        Plan memory plan = plans[user.deposits[idx].plan];
        
        uint256 principal = user.deposits[idx].amount;
        uint256 intended_reward = principal.mul(plan.percent)
                                            .mul(plan.time)
                                            .div(100)
                                            .div(365);

        totalStaked -= principal;
        plans[user.deposits[idx].plan].currentStake -= principal;
        delete users[msg.sender].deposits[idx];
        user.depositIdx = remove(idx, user.depositIdx);
        
        require(principal > 0, "Nothing to unstake");
        IGLAToken(glaTokenAddress).burn(intended_reward);
        
        user.numberOfDeposits -= 1;

        IGLAToken(glaTokenAddress).transfer(
            msg.sender,
            principal
        );

        emit UnstakeBeforeMaturity(msg.sender, idx, principal, block.timestamp);
    }

    function _afterMaturity(uint256 idx) public view returns(bool) {
        User storage user = users[msg.sender];
        Plan memory plan = plans[user.deposits[idx].plan];

        return block.timestamp >= user.deposits[idx].start + plan.time.mul(TIME_STEP);
    }

    function adminEmergencyWithdraw() public onlyOwner {
        address glaTokenAddress = IGameManager(gameManager).getContract(
            "GLAToken"
        );
        IGLAToken(glaTokenAddress).transfer(
            msg.sender,
            IGLAToken(glaTokenAddress).balanceOf(address(this))
        );
    }

    function getPlanInfo(uint8 plan)
        public
        view
        returns (uint256 time, uint256 percent)
    {
        time = plans[plan].time;
        percent = plans[plan].percent;
    }

    function getUserNumberOfDeposits(address userAddress)
        public
        view
        returns (uint256)
    {
        return users[userAddress].numberOfDeposits;
    }

    function getUserTotalDeposits(address userAddress)
        public
        view
        returns (uint256 amount)
    {
        for (uint256 i = 0; i < users[userAddress].numberOfDeposits; i++) {
            uint256 idx = users[userAddress].depositIdx[i];
            amount = amount.add(users[userAddress].deposits[idx].amount);
        }
    }

    function getUserDepositInfo(address userAddress)
        public
        view
        returns (DepositInfo[] memory)
    {
        DepositInfo[] memory result = new DepositInfo[](
            users[userAddress].depositIdx.length
        );
        
        User storage user = users[userAddress];

        for (uint256 i; i < user.depositIdx.length; i++) {
            uint256 idx = user.depositIdx[i];
            result[i].depositIndex = user.deposits[idx].index;
            result[i].amountDeposit = user.deposits[idx].amount;
            result[i].plan = user.deposits[idx].plan;
            result[i].percent = plans[result[i].plan].percent;
            result[i].start = user.deposits[idx].start;
            result[i].finish = user.deposits[idx].start.add(
                plans[user.deposits[idx].plan].time.mul(TIME_STEP)
            );

            uint256 interestRate = block.timestamp < result[i].finish
                ? block.timestamp.sub(result[i].start).div(TIME_STEP)
                : plans[user.deposits[idx].plan].time;

            result[i].amountGain = user
                .deposits[idx]
                .amount
                .mul(result[i].percent)
                .mul(interestRate)
                .div(100)
                .div(365);
        }
        return result;
    }

    function remove(uint256 _valueToFindAndRemove, uint256[] memory _array) internal pure returns(uint256[] memory) {
        uint256[] memory auxArray = new uint256[](_array.length - 1);
        
        uint256 i = 0;
        uint256 j = 0;
        
        while(i<_array.length){
            if(_array[i] != _valueToFindAndRemove){
                auxArray[j] = _array[i];
                j += 1;
                i += 1;
            }
            else{
                i+=1;
            }
        }
        
        return auxArray;
    }
    
    
    function setGLAMinter(address _glaMinter) public onlyOwner {
        glaMinter = _glaMinter;
    }

    function setGameManager(address _gameManager) public onlyOwner {
        gameManager = _gameManager;
    }
}

interface IGLAToken is IERC20 {
    function burn(uint256 amount) external;
}

interface IGLAMintOperator {
    function mint(address to, uint256 amount) external;
}

interface IGameManager {
    function getContract(string memory contract_)
        external
        view
        returns (address);
}
