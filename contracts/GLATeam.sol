// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

contract GLATeam is Ownable {
    struct TeamLeadInfo {
        uint256 depositBalance;
        uint256 totalClaimable;
        uint256 claimed;
    }
    using SafeMath for uint256;
    
    address teamWallet; 
    address glaTokenAddress;
    
    mapping(address=>bool) leaderAddresses; // Team leader addresses
    mapping(address=>TeamLeadInfo) leaderInfos;
    mapping(uint256 => uint256) unlockFundRate; // phase => unlock rate
       
    uint256 tgeUnlockTime; // PancakeSwap listing time
    uint256 unlockTimeLock = 30 days;

    bool public isUnlockClaim = false;
    
    constructor(address _glaTokenAddress, address _teamWallet){
        glaTokenAddress = _glaTokenAddress;
        teamWallet = _teamWallet;
        // 6% unlock per month after TGE
        unlockFundRate[0] = 0; 
        unlockFundRate[1] = 6;
        unlockFundRate[2] = 12;
        unlockFundRate[3] = 18;
        unlockFundRate[4] = 24;
        unlockFundRate[5] = 30;
        unlockFundRate[6] = 36;
        unlockFundRate[7] = 42;
        unlockFundRate[8] = 48;
        unlockFundRate[9] = 54;
        unlockFundRate[10] = 60;
        unlockFundRate[11] = 66;
        unlockFundRate[12] = 72;
        unlockFundRate[13] = 78;
        unlockFundRate[14] = 84;
        unlockFundRate[15] = 90;
        unlockFundRate[16] = 96;
        unlockFundRate[17] = 100;
        
        registLeaderAddress();
    }
    
    function withdrawPoolAmount() public onlyOwner{
        IERC20(glaTokenAddress).transfer(teamWallet, IERC20(glaTokenAddress).balanceOf(address(this)));
    }
    
    function unlockClaim() public onlyOwner{
        tgeUnlockTime = block.timestamp;
        isUnlockClaim = true;
    }
        
    function setGLAToken(address _glaTokenAddress) public onlyOwner{
        glaTokenAddress = _glaTokenAddress;
    }
    
    function setTeamWallet(address _teamWallet) public onlyOwner{
        teamWallet = _teamWallet;
    }
    
    // team leads can change their receiving address
    function changeReceiver(address _changeAddress) public{
        require(isLeaderAddress(_msgSender()),"Not in seed sale address");
        leaderAddresses[_msgSender()] = false;
        leaderAddresses[_changeAddress] = true;
        leaderInfos[_changeAddress] = leaderInfos[_msgSender()]; 
    }

    function registLeaderAddress()  internal {
        leaderAddresses[address(0x23A6Ba1B29990bDF3846b08018df77d734fA05b3)] = true ; // Dev
        leaderAddresses[address(0x55190075b6258da3806DdfCB24034f188b4a4308)] = true ; // Advisor
        leaderAddresses[address(0x7bCbBF7483B6625F60fa3eB81BC34F6A4133F03a)] = true ; // Team lead
        leaderAddresses[address(0xA304232b9FB4446eE4e1A3a02028E176ED875ac6)] = true ; // Ideas
        leaderAddresses[address(0xd13cADa769F71B1dDDd2DB604462627c258E3C8C)] = true ; // Finance 
        leaderAddresses[address(0x153cFEe8D57219fBC6AAa75369c55cC6a0A040Fc)] = true ; // Market
        leaderAddresses[address(0x9f601E04e2729F87054e1Cf66CDf5c3b2394D802)] = true ; // Design 
        leaderAddresses[address(0xD65C9fa28b6c6438d388cF76558C866CF4696969)] = true ; // airdrop 

        leaderInfos[address(0x23A6Ba1B29990bDF3846b08018df77d734fA05b3)].depositBalance = 9*3*10**23; //  30%
        leaderInfos[address(0x55190075b6258da3806DdfCB24034f188b4a4308)].depositBalance = 9*1*10**23; //  10%
        leaderInfos[address(0x7bCbBF7483B6625F60fa3eB81BC34F6A4133F03a)].depositBalance = 9*1*10**23; //  10%
        leaderInfos[address(0xA304232b9FB4446eE4e1A3a02028E176ED875ac6)].depositBalance = 9*1*10**23; //  10%
        leaderInfos[address(0xd13cADa769F71B1dDDd2DB604462627c258E3C8C)].depositBalance = 9*1*10**23; //  10%
        leaderInfos[address(0x153cFEe8D57219fBC6AAa75369c55cC6a0A040Fc)].depositBalance = 9*1*10**23; //  10%
        leaderInfos[address(0x9f601E04e2729F87054e1Cf66CDf5c3b2394D802)].depositBalance = 9*1*10**23; //  10%
        leaderInfos[address(0xD65C9fa28b6c6438d388cF76558C866CF4696969)].depositBalance = 9*1*10**23; //  10%
        
        leaderInfos[address(0x23A6Ba1B29990bDF3846b08018df77d734fA05b3)].totalClaimable = 9*3*10**23;
        leaderInfos[address(0x55190075b6258da3806DdfCB24034f188b4a4308)].totalClaimable = 9*1*10**23;
        leaderInfos[address(0x7bCbBF7483B6625F60fa3eB81BC34F6A4133F03a)].totalClaimable = 9*1*10**23;
        leaderInfos[address(0xA304232b9FB4446eE4e1A3a02028E176ED875ac6)].totalClaimable = 9*1*10**23;
        leaderInfos[address(0xd13cADa769F71B1dDDd2DB604462627c258E3C8C)].totalClaimable = 9*1*10**23;
        leaderInfos[address(0x153cFEe8D57219fBC6AAa75369c55cC6a0A040Fc)].totalClaimable = 9*1*10**23;
        leaderInfos[address(0x9f601E04e2729F87054e1Cf66CDf5c3b2394D802)].totalClaimable = 9*1*10**23;
        leaderInfos[address(0xD65C9fa28b6c6438d388cF76558C866CF4696969)].totalClaimable = 9*1*10**23;
    }
    
    function claim() public {
        require(isUnlockClaim, "Not unlock claim");
        require(isLeaderAddress(_msgSender()),"Not in leader address");
        
        uint256 unlockedAmount = getUnlockedAmount(_msgSender());
        
        // claimable amount
        uint256 availableForClaimAmount = availableClaim(_msgSender());
        require(availableForClaimAmount > 0, "Nothing for claim");
        
        // send to leader
        IERC20(glaTokenAddress).transfer(_msgSender(), availableForClaimAmount);

        // reset claimed tokens
        leaderInfos[_msgSender()].claimed = unlockedAmount;
    }
    
    function getTotalDeposit(address _address) public view returns(uint256){
        return leaderInfos[_address].depositBalance;
    }
    
    function getTotalClaimableBalance(address _address) public view returns(uint256){
        return leaderInfos[_address].totalClaimable;
    }
    
    function getNextTimestampClaim() public view returns(uint256){
        uint256 unlockFundRateIndex = block.timestamp.sub(tgeUnlockTime).div(unlockTimeLock).add(1);
        return unlockFundRateIndex.mul(unlockTimeLock).add(tgeUnlockTime);
    }
    
    function isLeaderAddress(address _address) public view returns(bool){
        return leaderAddresses[_address];
    }
    
    function availableRemainClaimable(address _address) public view returns(uint256){
        return  leaderInfos[_address].totalClaimable.sub(leaderInfos[_address].claimed);
    }
    
    function availableClaim(address _address) public view returns(uint256){
        uint256 unlockedAmount = getUnlockedAmount(_address);
        return unlockedAmount - leaderInfos[_address].claimed;
    }
    
    function getUnlockedAmount(address _address) internal view returns(uint256){
        // get index of unlockFundRate
        uint256 unlockFundRateIndex = block.timestamp.sub(tgeUnlockTime).div(unlockTimeLock);
        
        // release all in 17 months 
        if(unlockFundRateIndex > 17) 
            unlockFundRateIndex = 17;

        // return rate * claimable tokens
        return leaderInfos[_address].totalClaimable.mul(unlockFundRate[unlockFundRateIndex]).div(100);
    }
}

