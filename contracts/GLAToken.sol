// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Capped.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/utils/Address.sol";

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

abstract contract BPContract {
    function protect(
        address sender,
        address receiver,
        uint256 amount
    ) external virtual;
}
contract GLAToken is ERC20Capped, ERC20Burnable, Ownable {
    // BPContact
    BPContract public BP;
    bool public bpEnabled;

    event BPAdded(address indexed bp);
    event BPEnabled(bool indexed _enabled);
    event BPTransfer(address from, address to, uint256 amount);

    uint public blacklistTime;

    mapping (address => bool) public blacklist ;

    using SafeMath for uint256;
    address operator;
    mapping(address=>bool) lpAddresses;
    uint256 maxSupply = 10**27;
    uint256 amountPlayToEarn = 410 * 10**24;
    mapping(address=>bool) excludeFee;
    uint256 buyFeeRate = 2;
    uint256 sellFeeRate = 5;
    address marketingAddress;
    constructor()
        Ownable()
        ERC20Capped(maxSupply)
        ERC20("Galaxy Adventure", "GLA")
    {
        blacklistTime = block.timestamp + 3 days;
        marketingAddress = _msgSender();
        ERC20._mint(_msgSender(),maxSupply - amountPlayToEarn);
    }

    modifier onlyOperator() {
        require(operator == _msgSender(), "Operator: caller is not the operator");
        _;
    }

    function mint(address account, uint256 amount) external onlyOperator {
        _mint(account, amount);
    }

    function _transfer(address sender,address recipient, uint256 amount) internal override(ERC20){
        require(!blacklist[sender] && !blacklist[recipient], "the address is blacklisted");
        if (bpEnabled) {
            BP.protect(sender, recipient, amount);
            emit BPTransfer(sender, recipient, amount);
        }

        require(balanceOf(sender) >= amount,"balance >= amount ");
        uint256 feeRate = 0;
        if(lpAddresses[sender]){//buy
            feeRate = buyFeeRate;
        }
        else if(lpAddresses[recipient]){//sell
            feeRate = sellFeeRate;
        }
        //
        if(feeRate>0 && !excludeFee[sender] && !excludeFee[recipient] && recipient != marketingAddress){
            uint256 fee = amount.mul(feeRate).div(100);
            amount = amount.sub(fee);
            ERC20._transfer(sender,recipient,amount);
            ERC20._transfer(sender,marketingAddress,fee);
        }
        else {
            ERC20._transfer(sender,recipient,amount);
        }
    }
    function _mint(address account, uint256 amount) internal virtual override (ERC20, ERC20Capped) {
        ERC20Capped._mint(account, amount);
    }
    
    function setOperator(address _operator) external onlyOwner {
        operator = _operator;
    }
    function setLpAddress(address _lpAddress,bool _isLp) external onlyOwner{
        lpAddresses[_lpAddress] = _isLp;
    }
    function setExcludeFee(address[] memory _addresses,bool _exclude) external onlyOwner{
        for(uint i=0;i<_addresses.length;i++){
              excludeFee[_addresses[i]] = _exclude;
        }
    }
    function setMarketingAddress(address _marketingAddress) external onlyOwner{
        marketingAddress = _marketingAddress;
    }
    function setBpAddress(address _bp) external onlyOwner {
        require(address(BP) == address(0), "Can only be initialized once");
        BP = BPContract(_bp);

        emit BPAdded(_bp);
    }

    function setBpEnabled(bool _enabled) external onlyOwner {
        require(address(BP) != address(0), "You have to set BP address first");
        bpEnabled = _enabled;
        emit BPEnabled(_enabled);
    }
    
    function multiBlacklist(address[] memory addresses) external onlyOwner {

        require(block.timestamp <= blacklistTime, "blacklistTime: invalid");

        for (uint256 i = 0; i < addresses.length; i++) {
            blacklist[addresses[i]] = true;
        }

    }

    function multiRemoveFromBlacklist(address[] memory addresses) external onlyOwner {
        for (uint256 i = 0; i < addresses.length; i++) {
            blacklist[addresses[i]] = false;
        }
    }
}