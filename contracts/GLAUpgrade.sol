//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/access/Ownable.sol";

contract GLAUpgrade is Ownable{
    uint256 constant BASE_GEM_AMOUNT = 100;
    uint256 constant RANDOM_RANGE = 10000;
    mapping (uint256 => uint256) ratioOfRare; // Current rarity => Upgrade success rate

    address gameManager;
    
    event Upgrade(uint256 indexed heroId, bool status);

    constructor(address gameManager_) Ownable() {
        ratioOfRare[1] = 4194;
        ratioOfRare[2] = 2581;
        ratioOfRare[3] = 1613;
        ratioOfRare[4] = 968;
        ratioOfRare[5] = 645;
        gameManager = gameManager_;
    }

    function upgradeHero(uint256 heroId, bool isUseCore) public returns(bool) {
        require(msg.sender == tx.origin, "don't try to cheat");
        address heroContract = IGameManager(gameManager).getContract("GLAHeroNFT");

        require(msg.sender == IGLAHeroNFT(heroContract).ownerOf(heroId),"You not own this hero");
        
        address itemContract = IGameManager(gameManager).getContract("GLAItem");
        uint8 rare = IGLAHeroNFT(heroContract).getHeroRarity(heroId);

        if (rare == 1){
            require(isUseCore, "You have to use core to update hero 1 star!");
        }

        if(isUseCore) // burn 1 core (tokenid = 2)
        {
            IGLAItem(itemContract).burn(msg.sender, 2, 1);
        }

        // Gems (tokenid = 1) needed to upgrade = BASE_GEM_AMOUNT * rarity
        IGLAItem(itemContract).burn(msg.sender, 1, BASE_GEM_AMOUNT*rare);
        uint256 rnd = _random(heroId, RANDOM_RANGE);
        
        if (rnd < ratioOfRare[rare]){
            IGLAHeroNFT(heroContract).upgradeRarity(heroId);
            emit Upgrade(heroId, true);
            return true;
            }

        else{
            if (!isUseCore)
            {
                IGLAHeroNFT(heroContract).downgradeRarity(heroId);
            }
            emit Upgrade(heroId, false);
            return false;
            }
        }

        
    function _random(uint256 nonce, uint256 range) internal view returns (uint256)  {
        return uint256(keccak256(abi.encodePacked(msg.sender, blockhash(block.number -1), nonce, block.gaslimit, block.coinbase, block.timestamp , gasleft())))%range;      
    }
        
    function setGameManager(address gameManager_) public onlyOwner{
        gameManager = gameManager_;
    }
}

interface IGameManager{
    function  getContract(string memory contract_) external view returns (address);
}

interface IGLAHeroNFT{
    function getHeroRarity(uint256 tokenId_) external view returns (uint8);
    function downgradeRarity(uint256 tokenId_) external;
    function upgradeRarity(uint256 tokenId_) external;
    function ownerOf(uint256 tokenId) external view returns (address);
}

interface IGLAItem{
    function burn(address account, uint256 id, uint256 amount) external;
}
