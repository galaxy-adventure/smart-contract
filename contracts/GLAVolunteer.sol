// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

contract GLAVolunteer is Ownable {
    struct VolunteerInfo {
        address wallet;
        uint256 wageClaimable;
        uint256 wageLocked;
        uint256 claimed;
    }
    using SafeMath for uint256;
    // Unlock haftpart of wage after 15 days
    uint256 constant WAGE_UNLOCK_TIME = 15 days;
    uint256 payDay;
    address constant GLA_ADDRESS = 0x292Bb969737372E48D97C78c19B6a40347C33B45; //
    mapping(address => bool) volunteerAddresses; // Addresses of volunteers
    mapping(address => VolunteerInfo) volunteerInfos;

    constructor() {}

    // seedsaler can change their address to receive GLA
    function changeReceiver(address _changeAddress) public {
        //verify seedsale address
        require(isVolunteer(_msgSender()), "You are not a volunteer");

        // delete old address
        volunteerAddresses[_msgSender()] = false;

        // add replaced address
        volunteerAddresses[_changeAddress] = true;

        // move info from old to new address
        volunteerInfos[_changeAddress] = volunteerInfos[_msgSender()];
    }

    function addNewVolunteers(
        address[] memory _volunteerAddresses
    ) public onlyOwner {
        for (uint256 i = 0; i < _volunteerAddresses.length; i++) {
            if (!isVolunteer(_volunteerAddresses[i])) {
                volunteerAddresses[_volunteerAddresses[i]] = true;
                volunteerInfos[_volunteerAddresses[i]] = VolunteerInfo(
                    _volunteerAddresses[i],
                    0,
                    0,
                    0
                );
            }
        }
    }

    function setVolunteersWage(
        address[] memory _volunteerAddresses,
        uint256[] memory wageAmount
    ) public onlyOwner {
        require(
            _volunteerAddresses.length == wageAmount.length,
            "One wageAmount per volunteer"
        );
        for (uint256 i = 0; i < _volunteerAddresses.length; i++) {
            volunteerInfos[_volunteerAddresses[i]]
                .wageClaimable += volunteerInfos[_volunteerAddresses[i]].wageLocked
                .add(wageAmount[i].div(2));
            volunteerInfos[_volunteerAddresses[i]].wageLocked = wageAmount[i]
                .div(2);
        }
        payDay = block.timestamp;
    }

    function claim() public {
        require(isVolunteer(_msgSender()), "You are not a volunteer");
        uint256 availableWage = getTotalClaimableBalance(_msgSender());
        require(availableWage > 0, "Nothing to claim");
        volunteerInfos[_msgSender()].wageClaimable = 0;
        if (block.timestamp >= payDay + WAGE_UNLOCK_TIME) {
            volunteerInfos[_msgSender()].wageLocked = 0;
        }
        IERC20(GLA_ADDRESS).transfer(_msgSender(), availableWage);
        volunteerInfos[_msgSender()].claimed += availableWage;
    }

    function getTotalClaimableBalance(address _address)
        public
        view
        returns (uint256)
    {
        uint256 totalClaimable = block.timestamp >= payDay + WAGE_UNLOCK_TIME
            ? volunteerInfos[_address].wageClaimable.add(
                volunteerInfos[_address].wageLocked
            )
            : volunteerInfos[_address].wageClaimable;
        return totalClaimable;
    }

    function emergencyWithdraw() public onlyOwner {
        IERC20(GLA_ADDRESS).transfer(
            owner(),
            IERC20(GLA_ADDRESS).balanceOf(address(this))
        );
    }
    
    function withdrawERC20Token(address _tokenAddress) public onlyOwner{
        IERC20(_tokenAddress).transfer(
            owner(),
            IERC20(_tokenAddress).balanceOf(address(this))
            );
    }
    
    function withdrawBNB() public onlyOwner{
        address payable _owner = payable(owner());
        _owner.transfer(address(this).balance);
    }

    function getNextTimestampClaim() public view returns (uint256) {
        return payDay + WAGE_UNLOCK_TIME;
    }

    function isVolunteer(address _address) public view returns (bool) {
        return volunteerAddresses[_address];
    }

    function wageLocked(address _address) public view returns (uint256) {
        return
            block.timestamp >= payDay + WAGE_UNLOCK_TIME
                ? 0
                : volunteerInfos[_address].wageLocked;
    }

    function totalClaimed(address _address) public view returns (uint256) {
        return volunteerInfos[_address].claimed;
    }
}
