//  SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
contract GameManager is Ownable, Pausable{
    
    address private devWallet;
    string[] private listContractName;
    mapping (string => address) addressOf; // contract name => its address
    constructor(){
        listContractName = ["GLAHeroNFT", "GLAToken", "GLAMarket", "GLASpawner", "GLABattle", "GLAUpgrade", "GLAItem", "GLAEquipment","GLAFarming","GLAP2P"];
    }

    function getListContract() public view returns (string[] memory){
        return listContractName;
    }

    function  getDevWallet() external view returns (address){
        return devWallet;
    }

    function setDevWallet(address devWallet_) public onlyOwner{
        devWallet = devWallet_;
    }

    function getContract(string memory contract_) external whenNotPaused view returns(address){
        return addressOf[contract_];
    }
    
    function setContract(string memory contract_, address address_) external onlyOwner {
        addressOf[contract_] = address_;
    }
    
    function pause() public whenNotPaused onlyOwner{
        _pause();
    }
    
    function unpause() public whenPaused onlyOwner{
        _unpause();
    }
    //GLAMarket
    
    function marketSetMarketFee(uint8 newFee) public onlyOwner{
        IGLAMarket(addressOf["GLAMarket"]).setFee(newFee);
    }
    
    //GLABattle
    function battleSetBaseRate(uint256 _heroLvBaseRate, uint256 _evilLvBaseRate, uint256 _rareBaseRate) public onlyOwner{
        IGLABattle(addressOf["GLABattle"]).setBaseRate(_heroLvBaseRate, _evilLvBaseRate, _rareBaseRate);
    }
    
    function battleSetBaseTokenReward(uint8 _baseTokenReward) public onlyOwner{
        IGLABattle(addressOf["GLABattle"]).setBaseTokenReward(_baseTokenReward);
    }
    
    function battleSetBaseExpReward(uint256 _baseExpReward) public onlyOwner{
        IGLABattle(addressOf["GLABattle"]).setBaseExpReward(_baseExpReward);
    }
    
    function battleSetWinRate(uint8 evilLevel, uint8 value) public onlyOwner{
        IGLABattle(addressOf["GLABattle"]).setWinRate(evilLevel, value);
    }
    
    function battleSetCoolDown(uint8 rarity, uint256 minute) public onlyOwner{
        IGLABattle(addressOf["GLABattle"]).setCoolDown(rarity, minute);
    }
    
    function battleSetWeightOfEvilLv(uint8 _evilLevel, uint256 _weightOfEvilLv) public onlyOwner{
        IGLABattle(addressOf["GLABattle"]).setWeightOfEvilLv(_evilLevel, _weightOfEvilLv);
    }
    
    function battleSetBoostInfo(uint256 _boostTimestamp, uint256 _boostLast, uint256 _boostRateExp, uint256 _boostRateToken) public onlyOwner{
        IGLABattle(addressOf["GLABattle"]).setBoostInfo(_boostTimestamp, _boostLast, _boostRateExp, _boostRateToken);
    }
    
    //GLAItem
    function itemCreateNewItemType(string memory newTypeItem, uint256 price) public onlyOwner {
        IGLAItem(addressOf["GLAItem"]).createNewItemType(newTypeItem, price);
    }
    
    function itemChangeItemPrice(string memory itemType, uint256 price) public onlyOwner{
        IGLAItem(addressOf["GLAItem"]).changeItemPrice(itemType, price);
    }
    
    //GLASpawner
    
    function spawnerSetPriceNewHero(uint256 newHeroPrice) public onlyOwner{
        IGLASpawner(addressOf["GLASpawner"]).setPrice(newHeroPrice);
    }
    
    //GLAHeroNFT
    function heroNFSetRenameFee(uint256 newRenameFee) public onlyOwner{
        IGLAHeroNFT(addressOf["GLAHeroNFT"]).setRenamFee(newRenameFee);
    }
}

interface IGLAMarket{
    function setFee(uint8 fee) external;
}

interface IGLABattle{
    function setBaseRate(uint256 _heroLvBaseRate, uint256 _evilLvBaseRate, uint256 _rareBaseRate) external;
    function setBaseTokenReward(uint8 _baseTokenReward) external;
    function setBaseExpReward(uint256 _baseExpReward) external;
    function setWinRate(uint8 evilLevel, uint8 value) external;
    function setCoolDown(uint8 rarity, uint256 minute) external;
    function setWeightOfEvilLv(uint8 _evilLevel, uint256 _weightOfEvilLv) external;
    function setBoostInfo(uint256 _boostTimestamp, uint256 _boostLast, uint256 _boostRateExp, uint256 _boostRateToken) external;
}

interface IGLAItem{
    function createNewItemType(string memory newTypeItem, uint256 price) external;
    function changeItemPrice(string memory itemType, uint256 price) external;
}

interface IGLASpawner{
        function setPrice(uint256 newHeroPrice) external;
}
interface IGLAHeroNFT{
    function setRenamFee(uint256 newRenameFee) external;
}
