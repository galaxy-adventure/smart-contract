// Requiring the module
const Web3 = require('web3')
const reader = require('xlsx')
const Tx = require('ethereumjs-tx')

//Initial BSC
const rpcURL = '' // Your RCkP URL goes here
const web3 = new Web3(rpcURL)
const account = '' // Your account address 
const privateKey = Buffer.from('', 'hex') // Your private key

// Read the deployed contract - get the addresss from BSC
const contractAddress = '0xc1ff39D8BB3B13d6d22ab37F22CC77A0222A093B'
const contractABI = [
	{
		"inputs": [
			{
				"internalType": "address[]",
				"name": "_addresses",
				"type": "address[]"
			},
			{
				"internalType": "bool",
				"name": "_isWL",
				"type": "bool"
			}
		],
		"name": "addWhiteListAddr",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "_address",
				"type": "address"
			}
		],
		"name": "isWhitelisted",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "retrieve",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "num",
				"type": "uint256"
			}
		],
		"name": "store",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	}
]
const contract = new web3.eth.Contract(contractABI, contractAddress)

// Reading whitelist excel file
const file = reader.readFile('./test.xlsx')
const worksheet = file.Sheets[file.SheetNames[0]]
let columnA = []
const BATCH_SIZE = 20 // 20 is best for testnet to optimize gas

function sleep(ms) {
	return new Promise((resolve) => {
		setTimeout(resolve, ms);
	});
}

async function main() {
	try {

		//Read excel before
		for (let firstColumn in worksheet) {
			if (firstColumn.toString()[0] === 'A') {
				columnA.push(worksheet[firstColumn].v)
			}
		}

		//Calculate batch
		let numberLoop = Math.floor(columnA.length / BATCH_SIZE) + 1
		for (let i = 0; i < numberLoop; i++) {
			let start = i * BATCH_SIZE
			let end = 0
			if ((start + BATCH_SIZE) > columnA.length) {
				end = columnA.length
			} else {
				end = start + BATCH_SIZE
			}
			let batchData = columnA.slice(start, end)
			contract.methods.retrieve().call((err, result) => { console.log("Before insert " + result) })
			console.log(batchData)

			//Insert to smartcontract
			web3.eth.getTransactionCount(account, (err, txCount) => {
				const txObject = {
					nonce: web3.utils.toHex(txCount),
					gasLimit: web3.utils.toHex(800000), // Raise the gas limit to a much higher amount
					gasPrice: web3.utils.toHex(web3.utils.toWei('10', 'gwei')),
					to: contractAddress,
					data: contract.methods.addWhiteListAddr(batchData, true).encodeABI()
				}

				const tx = new Tx(txObject)
				tx.sign(privateKey)

				const serializedTx = tx.serialize()
				const raw = '0x' + serializedTx.toString('hex')

				web3.eth.sendSignedTransaction(raw, (err, txHash) => {
					console.log('err:', err, 'txHash:', txHash)
					// Use this txHash to find the contract on Etherscan!
				})
			})
			await sleep(10000)
			contract.methods.retrieve().call((err, result) => { console.log("After inser " + result) })
		}
		console.log("DONE - Import data successfully!")
	} catch (e) {
		console.log(e)
	}
}

main()
